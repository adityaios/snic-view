//
//  main.m
//  Snic View
//
//  Created by Aditya Kumar on 2/27/16.
//  Copyright © 2016 wry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
