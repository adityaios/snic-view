//
//  AppDelegate.h
//  Snic View
//
//  Created by Aditya Kumar on 2/27/16.
//  Copyright © 2016 wry. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

